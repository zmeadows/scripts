from ROOT import *

import argparse
import sys

#################
# CONFIGURATION #
#################

parser = argparse.ArgumentParser(description='VBF Selector')
parser.add_argument('--input'     , nargs='+', help='an ntuple to run over')
parser.add_argument('--trees'     , nargs='+', help='name of tree(s) in input ntuple to run over')
parser.add_argument('--selection' , nargs='+', help='The resulting file ')
parser.add_argument('--output'    , help='The resulting root file containing all histograms')

args = parser.parse_args()

print "\n\tVBF SELECTOR\n"

print "ARGUMENTS:"
print "============================"
print "input     : " , args.input
print "tree      : " , args.trees
print "output    : " , args.output
print "selection : " , args.selection
print "============================\n"

def FATAL_ERROR(error_msg):
    print "ERROR:", error_msg
    sys.exit(1)

output_file = TFile.Open(args.output, "RECREATE")

# check that input files exist and possess user-specified trees
for input_file_name in args.input:
    input_file = TFile.Open(input_file_name, "READ")
    if (not input_file):
        FATAL_ERROR("input file: " + input_file_name + " could not be opened.")
    for tree_name in args.trees:
         tree = input_file.Get(tree_name)
         if (not tree):
            FATAL_ERROR("input file: " + input_file_name + " does not contain the requested tree: " + tree_name)

##############
# SELECTIONS #
#############

class Selection:
    def __init__(self, name):
        self.name = name
        self.cutflow = {}

    def check(self, event):
        return False

    def bump(self, cut_name):
        if cut_name in cutflow_map:
            cutflow_map[cut_name] = cutflow_map[cut_name] + 1
        else:
            cutflow_map[cut_name] = 1

class VVJJ_PRESEL(Selection):
    def __init__(self):
        Selector.__init__(self, "VVJJ_PRESEL"):
    def check(self, event):
        return True

class VVJJ_WW(Selection):
    def __init__(self):
        Selector.__init__(self, "VVJJ_WW"):
    def check(self, event):
        return True

class VVJJ_WZ(Selection):
    def __init__(self):
        Selector.__init__(self, "VVJJ_WZ"):
    def check(self, event):
        return True

class VVJJ_ZZ(Selection):
    def __init__(self):
        Selector.__init__(self, "VVJJ_ZZ"):
    def check(self, event):
        return True

selections = {}

def add_selection(sel):
    global selections
    selections[sel.name] = sel


##############
# EVENT LOOP #
##############

def loop(tree, selection):
    i = 0
    for event in tree:
        print event.jet_pt[0]
        i = i + 1
        if (i > 0): break

# do the actual processing
for tree_name in args.trees:
    for input_file_name in args.input:
        input_file = TFile.Open(input_file_name, "READ")
        tree = input_file.Get(tree_name)
        loop(tree,0)
