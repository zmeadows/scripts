#! /bin/bash
#!-----------------------------------------------------------------------------
#! Setup the ssh agent.
#!
#!  This is copied from http://mah.everybody.org/docs/ssh
#!   and modified appropriately.
#!
#!  Usage:
#!   Put this file in the ~/bin directory and do once per host
#!     source sshAgent.sh
#!   Next, also once, add the appropriate ssh key file and enter the password.
#!     ssh-add $HOME/.ssh/<myKeyFile>
#!   From now on, you will not be prompted for the password.
#!
#!-----------------------------------------------------------------------------

envDir=$HOME/.ssh/env
mkdir -p $envDir
SSH_ENV="$envDir/environment".`hostname -s`

function start_agent {
    echo "Initialising new SSH agent..."
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "$SSH_ENV"
    echo succeeded
    chmod 600 "$SSH_ENV"
    . "$SSH_ENV" > /dev/null
    /usr/bin/ssh-add;
}

# Source SSH settings, if applicable

if [ -f "$SSH_ENV" ]; then
    agentPid=`grep "Agent pid" $SSH_ENV | sed 's/.*pid \([0-9]*\);/\1/g'`
    result=`ps -p $agentPid`
    rc=$?
    if [ $rc -ne 0 ]; then
    rm -f $SSH_ENV
    start_agent;
    else
    . "$SSH_ENV" > /dev/null
    ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
        start_agent;
    }
    fi
else
    start_agent;
fi
