#!/usr/bin/python2

import os
import sys
import re
import subprocess

if (len(sys.argv) < 3):
    print "ERROR: requires at least two arguments"
    print "\t EXAMPLE: python get_NET2_local_filepaths.py NET2_DATADISK mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e3581_s2578_s2195"
    sys.exit(1)

MY_RSE = sys.argv[1]
MY_SAMPLENAME = sys.argv[2]

command_to_attempt = [
    "rucio",
    "list-file-replicas",
    "--rse",
    MY_RSE,
    MY_SAMPLENAME
    ]

try:
    raw_rucio_output = subprocess.check_output(' '.join(command_to_attempt), stderr=subprocess.STDOUT, shell=True)
except:
    sys.exit(1)


local_filepaths = []

for line in raw_rucio_output.splitlines():
    # ignore formatting lines that rucio outputs
    if "server?SFN" not in line:
        continue

    # extract just the local filepath from each line
    line = line.split("server?SFN=")[1]
    line = line.split(' ')[0]

    # just in something screwed up, maybe rucio output format changed
    if (not os.path.exists(line)):
        print "invalid file encountered: ", line
        sys.exit(1)

    local_filepaths.append(line)

if not os.path.exists(MY_SAMPLENAME):
    os.mkdir(MY_SAMPLENAME)
else:
    print "local directory called", MY_SAMPLENAME, "already exists!"
    print "Exiting here to be safe. Remove the directory and retry if you want."
    sys.exit(1)

for fp in local_filepaths:
    os.system(' '.join(["ln", "-s", fp, MY_SAMPLENAME + "/" + os.path.basename(fp)]))

